import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailDaftarTransaksiPageRoutingModule } from './detail-daftar-transaksi-routing.module';

import { DetailDaftarTransaksiPage } from './detail-daftar-transaksi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailDaftarTransaksiPageRoutingModule
  ],
  declarations: [DetailDaftarTransaksiPage]
})
export class DetailDaftarTransaksiPageModule {}
