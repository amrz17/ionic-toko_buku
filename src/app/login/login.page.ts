import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../api/api-service.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  form = {
    username_tmu : '',
    password_tmu : ''
  }

  constructor(
    private router:Router,
    private api : ApiServiceService,
    private alert : AlertController
    ) { }

  private async presentAlert(title : any, message : any) {
    const alert = await this.alert.create({
      header: 'Login',
      subHeader: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  doPostLoginUser(){
    this.api.LoginUser(this.form)
            .subscribe( body => {
              const jsonResponse = JSON.parse(JSON.stringify(body));
              console.log(jsonResponse.id);
              console.log("Success ==> "+ JSON.stringify(body)); 
              this.presentAlert('Berhasil' , 'Anda Berhasil login')
              this.router.navigateByUrl('home'); 
            },
            err => {
              console.error('Gagal Login user ===> ', err.status);
              this.presentAlert('Gagal Login user', 'Login user gagal. Silahkan cek kembali jaringan internet anda.');
            });
  }

  doSubmitWithValidateFormInput(){

    var doSubmitForm = true;

    if(this.form.username_tmu == null || this.form.username_tmu == ''){
      this.presentAlert('Peringatan' , 'Anda Belum Input Data Email');
      doSubmitForm = false;
    }

    if(this.form.password_tmu == null || this.form.password_tmu == ''){
      this.presentAlert('Peringatan' , 'Anda Belum Input Data Password');
      doSubmitForm = false;
    }

    if(doSubmitForm){
      this.doPostLoginUser();
    }

    

  }


  toSignUp() {
    this.router.navigateByUrl('signup')
  }

  ngOnInit() {
  }

}
