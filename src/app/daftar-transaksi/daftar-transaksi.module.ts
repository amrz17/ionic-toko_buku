import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarTransaksiPageRoutingModule } from './daftar-transaksi-routing.module';

import { DaftarTransaksiPage } from './daftar-transaksi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarTransaksiPageRoutingModule
  ],
  declarations: [DaftarTransaksiPage]
})
export class DaftarTransaksiPageModule {}
