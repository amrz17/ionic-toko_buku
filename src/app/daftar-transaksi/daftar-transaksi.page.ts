import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
Router

@Component({
  selector: 'app-daftar-transaksi',
  templateUrl: './daftar-transaksi.page.html',
  styleUrls: ['./daftar-transaksi.page.scss'],
})
export class DaftarTransaksiPage implements OnInit {

  constructor(
    private router:Router
  ) { }

  doAkun() {
    this.router.navigateByUrl('profile')
  }

  ngOnInit() {
  }

}
