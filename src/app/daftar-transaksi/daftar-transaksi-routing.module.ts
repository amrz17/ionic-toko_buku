import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarTransaksiPage } from './daftar-transaksi.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarTransaksiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarTransaksiPageRoutingModule {}
