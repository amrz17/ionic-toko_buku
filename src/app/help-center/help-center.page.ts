import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-help-center',
  templateUrl: './help-center.page.html',
  styleUrls: ['./help-center.page.scss'],
})
export class HelpCenterPage implements OnInit {

  constructor(
    private router:Router
  ) { }

  doAkun() {
    this.router.navigateByUrl('profile')
  }


  ngOnInit() {
  }

}
