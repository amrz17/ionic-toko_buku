import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(
    private http : HttpClient
  ) { }


  LoginUser(form : any){
    return this.http.post(environment.ApiURL + '/api/login',
      {
        "username_tmu": form.username_tmu ,
        "password_tmu": form.password_tmu
      },
      { responseType: 'json'}
    );
  }

  CreateUser(form : any){
    return this.http.post(environment.ApiURL + '/api/register',
      {
        "nik_tmu": form.nik_tmu ,
        "name_tmu": form.name_tmu,
        "username_tmu" : form.username_tmu,
        "password_tmu" : form.password_tmu
      },
      { responseType: 'json'}
    );
  }

  GetListBooks(){
    return this.http.get(environment.ApiURL + '/api/show-book' , {} );
  }

  GetClassicLiterature() {
    return this.http.get(environment.ApiURL + '/api/show-book/Anak-Anak', {})
  }

  // GetListBooksByCategory() {
  //   return this.http.get(environment.ApiURL + '/api')
  // }
}
