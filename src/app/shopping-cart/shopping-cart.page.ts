import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
Router
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.page.html',
  styleUrls: ['./shopping-cart.page.scss'],
})
export class ShoppingCartPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  doHome() {
    this.router.navigateByUrl('home')
  }
}
