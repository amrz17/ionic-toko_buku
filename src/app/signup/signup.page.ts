import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  form = {
    nik_tmu : '',
    name_tmu : '',
    username_tmu : '',
    password_tmu : ''
  }

  constructor(
    private router:Router,
    private api : ApiServiceService ,
    private alert : AlertController
    ) { }

  private async presentAlert(title : any, message : any) {
    const alert = await this.alert.create({
      header: 'Sign Up',
      subHeader: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  doPostCreateUser(){
    this.api.CreateUser(this.form)
            .subscribe( body => {
              const jsonResponse = JSON.parse(JSON.stringify(body));
              console.log(jsonResponse.id);
              console.log("Success ==> "+ JSON.stringify(body)); 
              this.presentAlert('Berhasil' , 'Anda Berhasil Sign Up Dengan Benar'); 
            },
            err => {
              console.error('Gagal Create user ===> ', err.status);
              this.presentAlert('Gagal Create user', 'Create user gagal. Silahkan cek kembali jaringan internet anda.');
            });
  }

  doSubmitWithValidateFormInput(){

    var doSubmitForm = true;

    if(this.form.nik_tmu == null || this.form.nik_tmu == ''){
      this.presentAlert('Peringatan' , 'Anda Belum Input Data NIK');
      doSubmitForm = false;
    }

    if(this.form.name_tmu == null || this.form.name_tmu == ''){
      this.presentAlert('Peringatan' , 'Anda Belum Input Data Nama');
      doSubmitForm = false;
    }

    if(this.form.username_tmu == null || this.form.username_tmu == ''){
      this.presentAlert('Peringatan' , 'Anda Belum Input Data Email');
      doSubmitForm = false;
    }

    if(this.form.password_tmu == null || this.form.password_tmu == ''){
      this.presentAlert('Peringatan' , 'Anda Belum Input Data Password');
      doSubmitForm = false;
    }

    if(doSubmitForm){
      this.doPostCreateUser();
    }

  }

  ngOnInit() {
  }

  toLogin() {
    this.router.navigateByUrl('login') 
  }

}
