import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
Router

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishlistPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  doFavorite() {
    this.router.navigateByUrl('wishlist')
  }

  doAkun() {
    this.router.navigateByUrl('profile')
  }

  doHome() {
    this.router.navigateByUrl('home')
  }
}
