import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
Router

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {

  constructor(
    private router:Router
  ) { }

  doAkun() {
    this.router.navigateByUrl('profile')
  }
  
  ngOnInit() {
  }

}
