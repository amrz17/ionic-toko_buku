import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import Swiper from 'swiper';
import { ApiServiceService } from '../api/api-service.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'], 
})
export class HomePage {
  @ViewChild('swiper')
  swiperRef: ElementRef | undefined;
  swiper?: Swiper;

  DataBooks : any;
  ClassicLiterature : any;


  constructor(
    private router:Router,
    private api : ApiServiceService,
    private alert : AlertController
    ) {}

    GetAllBooks(){
      this.api.GetListBooks().subscribe( (res:any) =>{
        this.DataBooks = res;
        console.log('Data Books ===>'+JSON.stringify(res));
      });
    }

    GetClassicLiterature() {
      this.api.GetClassicLiterature().subscribe( (res:any) => {
        this.ClassicLiterature = res;
        console.log('Clasic Literature ===>'+JSON.stringify(res))
      });
    }


    ngOnInit() {
      this.GetAllBooks();
      this.GetClassicLiterature();
    }
  
//Swiper
  swiperReady() {
    this.swiper = this.swiperRef?.nativeElement.swiper;
  }

    goNext() {
      this.swiper?.slideNext();
    }

    goPrev() {
      this.swiper?.slidePrev();
    }

  swiperSlideChanged(e: any) {
    console.log('changed: ', e);
  }
//End of Swiper

  doFavorite() {
    this.router.navigateByUrl('wishlist')
  }

  doAkun() {
    this.router.navigateByUrl('profile')
  }

  doShoppingCart() {
    this.router.navigateByUrl('shopping-cart')
  }

  
}
