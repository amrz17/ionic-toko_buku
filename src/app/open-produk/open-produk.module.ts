import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpenProdukPageRoutingModule } from './open-produk-routing.module';

import { OpenProdukPage } from './open-produk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpenProdukPageRoutingModule
  ],
  declarations: [OpenProdukPage]
})
export class OpenProdukPageModule {}
