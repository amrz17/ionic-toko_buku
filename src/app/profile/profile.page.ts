import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    private router:Router) { }

  

  doFavorite() {
    this.router.navigateByUrl('wishlist')
  }

  doHome() {
    this.router.navigateByUrl('home')
  }

  doLogOut() {
    this.router.navigateByUrl('login')
  }

  toHelpCenter() {
    this.router.navigateByUrl('help-center')
  }

  toAboutUs() {
    this.router.navigateByUrl('about-us')
  }

  doDaftarTransaksi() {
    this.router.navigateByUrl('daftar-transaksi')
  }

  ngOnInit() {
  }
}
