import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'wishlist',
    loadChildren: () => import('./wishlist/wishlist.module').then( m => m.WishlistPageModule)
  },
  {
    path: 'shopping-cart',
    loadChildren: () => import('./shopping-cart/shopping-cart.module').then( m => m.ShoppingCartPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'daftar-transaksi',
    loadChildren: () => import('./daftar-transaksi/daftar-transaksi.module').then( m => m.DaftarTransaksiPageModule)
  },
  {
    path: 'detail-daftar-transaksi',
    loadChildren: () => import('./detail-daftar-transaksi/detail-daftar-transaksi.module').then( m => m.DetailDaftarTransaksiPageModule)
  },
  {
    path: 'open-produk',
    loadChildren: () => import('./open-produk/open-produk.module').then( m => m.OpenProdukPageModule)
  },
  {
    path: 'help-center',
    loadChildren: () => import('./help-center/help-center.module').then( m => m.HelpCenterPageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
